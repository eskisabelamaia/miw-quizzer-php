<?php

class Evaluator {
	private $quizzes;
	private $assessments;
	
	function __construct($quizzes, $assessments)
	{
		$this->quizzes = $quizzes;
		$this->assessments = $assessments;
	}
	
	public function evaluate()
	{
		$scores = array();
		$score = 0.0;
		foreach ($this->assessments as $studentId => $answers) {
			$score = $this->scoreCalculation($studentId, $answers);
			$scores[$studentId] = new Score($studentId, $score);
		}
		return $scores;
	}
	
	public function scoreCalculation($studentId, $answers)
	{
		$score = 0.0;
		foreach ($answers as $answer) {
			$score += $this->quizzes[$answer->getQuestion()]->getStudentAnswerScore($answer);
		}
		return $score;
	}
	
	public function compareScores($scores, $calculatedScores)
	{
		$counter = 0;
		$counterstudent = 0;
		
		print("Comparing the scores...\n");
		foreach ($scores as $score) {
			$counterstudent +=1;
			foreach ($calculatedScores as $calculatedScore) {
				if ($score->getStudentId() == $calculatedScore->getStudentId()){ 
					if($score->getValue() == $calculatedScore->getValue()) {$counter +=1;}
				}
			}
		}
		if ($counter == $counterstudent) {
			print("Calculated scores are equal\n");
			# Return for test
			return true;
		}else{
			# Return for test
			print("Calculated scores are not equal\n");
			return false;
		}
	}
	
	public function getAnswersStatistics() 
	{
		$statistics = array();
		
		foreach ($this->assessments as $studentId => $answers) {
			foreach ($answers as $answer) {
				if($this->quizzes[$answer->getQuestion()]->getStudentAnswerScore($answer) > 0) {
					if(array_key_exists($answer->getQuestion(), $statistics)) {
						$statistics[$answer->getQuestion()] += 1;
					} else {
						$statistics[$answer->getQuestion()] = 1;
					}
				}
			}
		}
	
		return $statistics;
	}
}