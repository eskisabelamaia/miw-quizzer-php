<?php

class Assessment {
	private $assessment;

	function __construct($assessment)
    {
        $this->assessment = $assessment;
    }

    public function getAssessment ()
    {
    	return $this->assessment;
    }

    public function setAssessment ($assessment) 
    {
    	$this->assessment = $assessment;
    }
}