<?php

class Score {
	private $studentId;
	private $value;
	
	function __construct($studentId, $value)
	{
		$this->grade = $value;
		$this->studentId = $studentId;
	}
	
	public function getValue()
	{
		return $this->grade;
	}
	
	public function setValue($value)
	{
		$this->grade = $value;
	}
	
	public function getStudentId()
	{
		return $this->studentId;
	}
	
	public function setStudentId($studentId)
	{
		$this->studentId = $studentId;
	}
}