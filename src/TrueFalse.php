<?php

require_once 'Question.php';

class TrueFalse extends Question
{
	private $id;
	private $questionText;
    private $correct;
    private $valueCorrect;
    private $valueIncorrect;
    private $feedback;

    function __construct($id, $text)
    {
        parent::__construct($id, $text);
    }
    
    public function getId()
    {
    	return $this->$id;
    }
    
    public function setId($id)
    {
    	$this->id = $id;
    }
    
    public function getQuestionText()
    {
    	return $this->questionText;
    }
    
    public function setQuestionText($questionText)
    {
    	$this->questionText = $questionText;
    }
    
    public function getCorrect()
    {
    	return $this->correct;
    }
    
    public function setCorrect($correct)
    {
    	$this->correct = $correct;
    }
        
    public function getValueOK()
    {
    	return $this->valueCorrect;
    }
    
    public function setValueOK($valueCorrect)
    {
    	$this->valueCorrect = $valueCorrect;
    }
    
    public function getValueFailed()
    {
    	return $this->valueIncorrect;
    }
    
    public function setValueFailed($valueIncorrect)
    {
    	$this->valueIncorrect = $valueIncorrect;
    }
    
    public function getFeedback()
    {
    	return $this->feedback;
    }
    
    public function setFeedback($feedback)
    {
    	$this->feedback = $feedback;
    }
    
    public function getStudentAnswerScore($answer) 
    {
		if($answer->getValue() == $this->getCorrect()) {
			return $this->getValueOK();
		} else {
			return $this->getValueFailed();
		}
    }
}