<?php

class Answer
{
    private $question;
    private $value;

    function __construct($question, $value)
    {
        $this->question = $question;
        $this->value = $value;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
} 