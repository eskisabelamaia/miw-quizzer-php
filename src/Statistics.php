<?php

class Statistics {
	private $statistics;
	
	function __construct($statistics)
	{
		$this->statistics = $statistics;
	}
	
	public function getStatistics()
	{
		return $this->statistics;
	}
	
	public function setStatistics($statistics)
	{
		$this->statistics = $statistics;
	}
	
	function writeStatistics($counter) {
		if($counter == 0) {
			file_put_contents("files/Statistics.json", json_encode($this->statistics)) or die("Unable to open file Statistics.json!");
		} else {
			file_put_contents("files/Statistics".$counter.".json", json_encode($this->statistics)) or die("Unable to open file Statistics.json!");
		}
	}
}