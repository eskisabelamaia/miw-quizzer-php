<?php

require_once 'Assessment.php';
require_once 'Deserialization.php';
require_once 'Evaluator.php';
require_once 'Quizz.php';
require_once 'Scores.php';
require_once 'Statistics.php';

class Quizzer {
		
	function main() {
		$files = getopt("f:");
		$file = explode(",", implode($files));
		switch (count($file)) {
			case 1:
				$counter = 1;
				$deserialization = new Deserialization();
				$fileManifest = json_decode(file_get_contents($files["f"]));
				foreach($fileManifest as $manifest) {
					foreach ($manifest as $subArray) {
						$strQuizz = json_decode(file_get_contents($subArray->quizz));
						$strAssessment = json_decode(file_get_contents($subArray->assessment));
						$strScores = json_decode(file_get_contents($subArray->scores));
						$quizz = new Quizz($deserialization->parseQuizzJson($strQuizz));
						$assessment = new Assessment($deserialization->parseAssessmentJson($strAssessment));
						$scores = new Scores($deserialization->parseScoresJson($strScores));
						$evaluator = new Evaluator($quizz->getQuestions(),$assessment->getAssessment());
						$calculatedScores = new Scores($evaluator->evaluate());
						print("Scores: {$counter}\n");
						$evaluator->compareScores($scores->getScores(), $calculatedScores->getScores());
						$statistics = new Statistics($evaluator->getAnswersStatistics());
						$statistics->writeStatistics($counter);
						print("Calculated statistics have been writen in the path files/Statistics".$counter.".json\n");
						$counter+=1;
					}
				}
				break;
			case 2:
				$deserialization = new Deserialization();
				$strQuizz = json_decode(file_get_contents($file[0]));
				$strAssessment = json_decode(file_get_contents($file[1]));
				$quizz = new Quizz($deserialization->parseQuizzJson($strQuizz));
				$assessment = new Assessment($deserialization->parseAssessmentJson($strAssessment));
				$evaluator = new Evaluator($quizz->getQuestions(),$assessment->getAssessment());
				$scores = new Scores($evaluator->evaluate());
				$scores->writeScores();
				print("Calculated scores have been writen in the path files/Scores.json\n");
				$statistics = new Statistics($evaluator->getAnswersStatistics());
				$statistics->writeStatistics(0);
				print("Calculated statistics have been writen in the path files/Statistics.json\n");
				break;
			default:
				echo "Invalid question type";
				break;
		}
	}
}

$quizzer = new Quizzer();
echo $quizzer->main();