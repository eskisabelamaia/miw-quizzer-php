<?php

require_once 'Assessment.php';
require_once 'Answer.php';
require_once 'Alternative.php';
require_once 'Quizz.php';
require_once 'Multichoice.php';
require_once 'TrueFalse.php';
require_once 'Scores.php';
require_once 'Score.php';

class Deserialization {
		
	public function parseQuizzJson ($strQuizz) {
        $questions = array();
        foreach ($strQuizz->questions as $question) {
        	switch ($question->type) {
        		case "multichoice":
        	    	$multichoice = new Multichoice($question->id, $question->questionText);
        	    	$alternatives = array();
           			foreach ($question->alternatives as $a) {
                		 $alternatives[] = new Alternative($a->code, $a->text, $a->value);
            		}
            		$alternative[$question->id] = $alternatives;
            		$multichoice->setAlternatives($alternative);  
            		$questions[$question->id] = $multichoice;
        			break;
        		case "truefalse":
        			$truefalse = new TrueFalse($question->id, $question->questionText);
		            $truefalse->setCorrect($question->correct);
		            $truefalse->setValueOK($question->valueOK);
		            $truefalse->setValueFailed($question->valueFailed);
		            $truefalse->setFeedback($question->feedback);
		            $questions[$question->id] = $truefalse;
        			break;
        		default:
        			echo "Invalid question type";
        			break;
        	}
        }
        return $questions;
	}
	
	public function parseAssessmentJson ($strAssessment) {
        $items = array();
        foreach ($strAssessment->items as $item) {
            $answer = array();
            foreach ($item->answers as $a) {
                $answer[] = new Answer($a->question, $a->value);
            }
            $items[$item->studentId] = $answer;
        }
        return $items;
	}
	
	public function parseScoresJson ($strScores) {
		$scores = array();
		foreach ($strScores->scores as $score) {
			$scores[$score->studentId] = new Score($score->studentId, $score->value);
		}
		return $scores;
	}
		
	public function parseManifestJson ($file) {

	}
}