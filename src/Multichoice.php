<?php

require_once 'Question.php';

class Multichoice extends Question
{
    private $alternatives;

    function __construct($id, $text)
    {
        parent::__construct($id, $text);

        $this->alternatives = array();
    }
    
    public function getAlternatives() {
    	return $alternatives;
    }
    
    public function setAlternatives($alternatives)
    {
    		$this->alternatives = $alternatives;
    }
    
    public function getStudentAnswerScore($answer)
    {
    	$score = 0.0;
		foreach ($this->alternatives[$answer->getQuestion()] as $alternative) {
			if($alternative->getCode() == $answer->getValue()) {
				$score = $alternative->getValue();
			}
		}
		return $score;
    }
}