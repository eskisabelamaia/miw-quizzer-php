<?php

class Alternative {
	var $code;
    var $text;
    var $value;

    function __construct($code, $text, $value)
    {
        $this->code = $code;
        $this->text = $text;
        $this->value = $value;
    }

    public function getCode ()
    {
    	return $this->code;
    }

    public function setCode ($code) 
    {
    	$this->code = $code;
    }

    public function getText ()
    {
    	return $this->text;
    }

    public function setText ($text) 
    {
    	$this->text = $text;
    }

    public function getValue ()
    {
    	return $this->value;
    }

    public function setValue ($value) 
    {
    	$this->value = $value;
    }

}