<?php
/**
 * Created by PhpStorm.
 * User: amaiaeskisabel
 * Date: 15/01/15
 * Time: 16:24
 */

class Scores {
	private $scores;
	
	function __construct($scores)
	{
		$this->scores = $scores;
	}
	
	public function getScores()
	{
		return $this->scores;
	}
	
	public function setScores($scores)
	{
		$this->scores = $scores;
	}
	
	function writeScores() {
		file_put_contents("files/Scores.json", json_encode($this->scores)) or die("Unable to open file Score.json!");
	}
}