<?php

abstract class Question
{
    private $questionText;
    private $id;

    function __construct($id, $questionText)
    {
        $this->id = $id;
        $this->text = $questionText;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getQuestionText()
    {
        return $this->questionText;
    }

    public function setQuestionText($questionText)
    {
        $this->text = $questionText;
    }

    abstract function getStudentAnswerScore ($answer);
}