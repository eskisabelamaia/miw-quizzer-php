<?php

class Quizz {
	private $questions;
	
	function __construct($questions)
	{
		$this->questions = $questions;
	}
	
	public function getQuestions()
	{
		return $this->questions;
	}
	
	public function setQuestions($questions)
	{
		$this->questions = $questions;
	}
}