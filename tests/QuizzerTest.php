<?php

require_once 'src/Assessment.php';
require_once 'src/Deserialization.php';
require_once 'src/Evaluator.php';
require_once 'src/Quizz.php';
require_once 'src/Scores.php';
require_once 'src/Statistics.php';

class QuizzerTest extends PHPUnit_Framework_TestCase
{
	private $deserialization;
	private $evaluator;
	private $quizz;
	private $assessment;
	private $fileQuizz;
	private $fileAssessment;
	private $fileManifest;

	public function __construct()
	{
		$this->deserialization = new Deserialization();
		$this->evaluator = new Evaluator();

		$this->fileQuizz = "Quizz.json";
		$this->fileAssessment = "Assessment.json";
		$this->fileManifest = "Manifest.json";
	}

	public function testQuizzerManifest()
	{
		$fileManifest = json_decode(file_get_contents($this->fileManifest));
		foreach($fileManifest as $manifest) {
			foreach ($manifest as $subArray) {
				$strQuizz = json_decode(file_get_contents($subArray->quizz));
				$strAssessment = json_decode(file_get_contents($subArray->assessment));
				$strScores = json_decode(file_get_contents($subArray->scores));
				$quizz = new Quizz($this->deserialization->parseQuizzJson($strQuizz));
				$assessment = new Assessment($this->deserialization->parseAssessmentJson($strAssessment));
				$scores = new Scores($this->deserialization->parseScoresJson($strScores));
				$evaluator = new Evaluator($quizz->getQuestions(),$assessment->getAssessment());
				$calculatedScores = new Scores($evaluator->evaluate());
				$evaluator->compareScores($scores->getScores(), $calculatedScores->getScores());
				
				$this->assertEquals($scores, $calculatedScores, 'Calculated scores are not correct.');
			}
		}
	}
	
	public function testQuizzerQuizzAssessment()
	{
		$scores = array();
		$scores["234"] = new Score(234, 0.75);
		$scores["245"] = new Score(245, 2);
		$scores["221"] = new Score(221, 0.75);
		
		
		$strQuizz = json_decode(file_get_contents($this->fileQuizz));
		$strAssessment = json_decode(file_get_contents($fileQuizz));
		$quizz = new Quizz($deserialization->parseQuizzJson($strQuizz));
		$assessment = new Assessment($deserialization->parseAssessmentJson($strAssessment));
		$evaluator = new Evaluator($quizz->getQuestions(),$assessment->getAssessment());
		$calculatedScores = new Scores($evaluator->evaluate());
		
		$this->assertEquals($scores, $calculatedScores, 'Calculated scores are not correct.');
	}
}